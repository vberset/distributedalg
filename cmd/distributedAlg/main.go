package main

import (
	da "distributedAlg"
	"fmt"
)

func main() {
	params := da.ParseParams()
	neighbors, nodes := da.ReadNeighborsList(params.NeighborsPath)

	fmt.Println("Neighbors:")
	for i, neighbor := range neighbors.Iter() {
		fmt.Println(i, neighbor)
	}

	server := da.StartUdpServer(params.Port)
	agent := da.NewTestConnectivityAgent(nodes, neighbors, params.IsProactive, server)
	agent.Run()
}
