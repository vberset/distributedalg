package distributedAlg

import (
	"fmt"
)

const HEADER = byte('D')

type Context struct {
	parent             *Node
	remainingNeighbors *NodeSet
}

func (ctx *Context) receivedFrom(neighbor Node) int {
	ctx.remainingNeighbors.Remove(neighbor)
	return ctx.remainingNeighbors.Size()
}

func isValidMessage(msg *IncomingMessage) bool {
	return msg.Payload[0] == HEADER && len(msg.Payload) > 1
}

func extractColor(msg *IncomingMessage) string {
	return string(msg.Payload[1:])
}

type TestConnectivityAgent struct {
	server          *Server
	color           string
	nodeCount       int
	terminatedCount int
	neighbors       *NodeSet
	contexts        map[string]*Context
	proactive       bool
	started         bool
}

func NewTestConnectivityAgent(nodeCount int, neighbors *NodeSet, proactive bool, server *Server) *TestConnectivityAgent {
	agent := new(TestConnectivityAgent)
	agent.server = server
	agent.color = server.BuildUniqueID()
	agent.nodeCount = nodeCount
	agent.terminatedCount = 0
	agent.neighbors = neighbors
	agent.proactive = proactive
	agent.contexts = make(map[string]*Context)
	agent.started = false
	return agent
}

func (agent *TestConnectivityAgent) createContextFrom(msg *IncomingMessage) *Context {
	context := new(Context)
	context.remainingNeighbors = agent.neighbors.Copy()

	if msg == nil {
		context.parent = nil
		agent.contexts[agent.color] = context
	} else {
		context.parent = &msg.Source
		agent.contexts[extractColor(msg)] = context
	}

	return context
}

func (agent *TestConnectivityAgent) getContextFrom(msg *IncomingMessage) (*Context, bool) {
	color := extractColor(msg)

	context, ok := agent.contexts[color]
	if !ok {
		context = agent.createContextFrom(msg)
	}

	return context, !ok
}

func (agent *TestConnectivityAgent) startOwnBroadcast() {
	agent.started = true
	agent.createContextFrom(nil)

	fmt.Println("Starts its own broadcast")
	payload := append([]byte{HEADER}, []byte(agent.color)...)
	for _, neighbor := range agent.neighbors.Iter() {
		agent.send(neighbor, payload)
	}
}

func (agent *TestConnectivityAgent) terminated() bool {
	return agent.nodeCount == agent.terminatedCount
}

func (agent *TestConnectivityAgent) send(neighbor Node, payload []byte) {
	agent.server.Out <- OutgoingMessage{
		neighbor,
		payload,
	}
}

func (agent *TestConnectivityAgent) Run() {
	defer agent.server.Shutdown()

	fmt.Println("TestConnectivity agent started")

	if agent.proactive {
		agent.startOwnBroadcast()
	}

	// Loop on received messages
	for {
		msg := <-agent.server.In
		if !isValidMessage(&msg) {
			fmt.Println("Received malformed message: ", msg)
			continue
		}

		// If not started, start it's own broadcast
		if !agent.started {
			agent.startOwnBroadcast()
		}

		// Retrieve or create the context of message's color
		context, new := agent.getContextFrom(&msg)
		remaining := context.receivedFrom(msg.Source)

		// If it's the first time it receives message of this color,
		// broadcast to neighbors
		if new {
			fmt.Println("Participate to new broadcast")
			for _, neighbor := range context.remainingNeighbors.Iter() {
				agent.send(neighbor, msg.Payload)
			}
		}

		// If has received message from all neighbors for this color,
		// acknowledge parent, but only if it's not its own color
		if remaining == 0 {
			if context.parent != nil {
				fmt.Println("Notify parent")
				agent.send(*context.parent, msg.Payload)
			}
			agent.terminatedCount += 1
		}

		// Check if all broadcast are down
		if agent.terminated() {
			fmt.Println("Algorithm terminated")
			return
		}
	}
}
