package distributedAlg

import (
	"math/rand"
)

type NodeSet struct {
	inner map[string]Node
}

func NewNodeSet() *NodeSet {
	return &NodeSet{
		make(map[string]Node),
	}
}

func (set *NodeSet) Add(node Node) {
	set.inner[node.HashKey()] = node
}

func (set *NodeSet) Remove(node Node) {
	delete(set.inner, node.HashKey())
}

func (set *NodeSet) Pop(node Node) Node {
	set.Remove(node)
	return node
}

func (set *NodeSet) PopRandom() Node {
	var node Node

	i := rand.Intn(set.Size())
	for _, node = range set.inner {
		if i == 0 {
			break
		}
		i--
	}

	set.Remove(node)
	return node
}

func (set *NodeSet) Contains(node Node) bool {
	_, c := set.inner[node.HashKey()]
	return c
}

func (set *NodeSet) Size() int {
	return len(set.inner)
}

func (set *NodeSet) Copy() *NodeSet {
	copy := NewNodeSet()

	for k, v := range set.inner {
		copy.inner[k] = v
	}

	return copy
}

func (set *NodeSet) Iter() []Node {
	var list []Node
	for _, node := range set.inner {
		list = append(list, node)
	}
	return list
}
