package distributedAlg

import (
	"fmt"
	"log"
	"math/rand"
	"net"
	"sync"
)

type IncomingMessage struct {
	Source  Node
	Payload []byte
}

type OutgoingMessage struct {
	Destination Node
	Payload     []byte
}

type Server struct {
	In     <-chan IncomingMessage
	Out    chan<- OutgoingMessage
	wg     sync.WaitGroup
	port   int
	socket *net.UDPConn
}

func (server *Server) Shutdown() {
	// By closing the Out channel, the udpSender closes the UDP socket and terminates,
	// leading to the termination of udpListener.
	close(server.Out)
	// Ensure both udpSender and udpListener are terminated.
	server.wg.Wait()
}

func StartUdpServer(port int) *Server {
	var server Server

	in := make(chan IncomingMessage, 100)
	out := make(chan OutgoingMessage, 100)

	server.In = in
	server.Out = out

	addr := new(net.UDPAddr)
	addr.IP = net.IPv4(0, 0, 0, 0)
	addr.Port = port

	socket, err := net.ListenUDP("udp4", addr)
	if err != nil {
		log.Fatal(err)
	}
	server.socket = socket
	server.port = port

	server.wg.Add(1)
	go udpListener(&server, in)
	server.wg.Add(1)
	go udpSender(&server, out)

	return &server
}

func (server *Server) BuildUniqueID() string {
	interfaces, _ := net.Interfaces()
	for i := len(interfaces) - 1; i >= 0; i-- {
		addresses, _ := interfaces[i].Addrs()
		if len(addresses) > 0 {
			return addresses[0].String()
		}
	}
	panic("No valid address")
}

func (server *Server) Send(msg OutgoingMessage) {
	server.wg.Add(1)

	go func() {
		defer server.wg.Done()

		// get random port
		port := 50000 + rand.Intn(10000)

		// Build local addr
		local := new(net.UDPAddr)
		local.IP = net.IP{0, 0, 0, 0}
		local.Port = port

		// Build remote addr
		remote := new(net.UDPAddr)
		remote.IP = msg.Destination.IP
		remote.Port = server.port

		// try to connect
		socket, err := net.DialUDP("udp4", local, remote)
		defer socket.Close()
		ensure(err)

		socket.Write(msg.Payload)
	}()
}

func udpListener(server *Server, in chan<- IncomingMessage) {
	defer server.wg.Done()

	for {
		buffer := make([]byte, 50)
		count, addr, err := server.socket.ReadFrom(buffer)
		if err != nil {
			return
		}

		host, _, err := net.SplitHostPort(addr.String())
		ensure(err)
		ips, err := net.LookupIP(host)
		ensure(err)

		msg := IncomingMessage{
			Node{
				ips[0],
			},
			buffer[:count],
		}

		fmt.Println("Received from ", msg.Source, " message ", msg.Payload)
		in <- msg
	}
}

func udpSender(server *Server, out <-chan OutgoingMessage) {
	defer server.socket.Close() // Close the socket to terminate updListener
	defer server.wg.Done()

	for {
		msg, more := <-out

		if more {
			server.Send(msg)
		} else {
			return
		}
	}
}
