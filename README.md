# Ditributed Alg

## Build

First, ensure dependencies are installed with `dep`

```terminal
dep ensure
```

then build the binary

```terminal
go build -o distributed_alg cmd/distributedAlg/main.go
```

## Deployement

The `deploy.sh` takes a folder as parameter. The folder must contain the SSH private key,
the list of servers `servers.txt`, and a `voisins-<id>.txt` for each server.

The number of node is hard coded to 6, due to the display mechanism.

```terminal
./deploy.sh dockerized
```
