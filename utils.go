package distributedAlg

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

const USAGE = "usage: <port> <neighbors file> WAIT|INIT"

const INIT = "INIT"
const WAIT = "WAIT"

type Params = struct {
	Port          int
	NeighborsPath string
	IsProactive   bool
}

func ParseParams() Params {
	args := os.Args[1:]

	if len(args) != 3 {
		log.Fatal(USAGE)
	}

	port, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatal(err)
	}

	path := args[1]

	var proactive bool

	switch args[2] {
	case WAIT:
		proactive = false
	case INIT:
		proactive = true
	default:
		log.Fatal(USAGE)
	}

	params := Params{
		port,
		path,
		proactive,
	}

	return params
}

type Node struct {
	IP net.IP
}

func (node *Node) HashKey() string {
	return fmt.Sprint(node.IP)
}

type LineReader struct {
	file    *os.File
	scanner *bufio.Scanner
}

func NewLineReader(path string) (*LineReader, error) {
	reader := new(LineReader)

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	reader.file = file
	reader.scanner = bufio.NewScanner(file)
	reader.scanner.Split(bufio.ScanLines)

	return reader, nil
}

func (reader *LineReader) Close() {
	reader.file.Close()
}

func (reader *LineReader) Next() bool {
	return reader.scanner.Scan()
}

func (reader *LineReader) Line() string {
	return reader.scanner.Text()
}

func ReadNeighborsList(path string) (*NodeSet, int) {
	reader, err := NewLineReader(path)
	if err != nil {
		log.Fatal("Unable to open ", path, ": ", err)
	}
	defer reader.Close()

	nodes := 0
	neighbors := NewNodeSet()

	for reader.Next() {
		line := reader.Line()

		if strings.HasPrefix(line, "nodes ") {
			nodes, err = strconv.Atoi(strings.TrimSpace(line[6:]))
			if err != nil {
				log.Fatal("malformed nodes count: ", err)
			}
			continue
		}

		addr, err := net.LookupIP(reader.Line())
		if err != nil {
			log.Fatal("malformed address: ", err)
		}
		neighbors.Add(
			Node{
				addr[0],
			},
		)
	}

	return neighbors, nodes
}

func ensure(err error) {
	if err != nil {
		log.Panic(err)
	}
}
