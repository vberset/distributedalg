#! /usr/bin/env bash
COUNT=6
SESSION=distributed
FOLDER=$1
SERVERS_FILE="$FOLDER/servers.txt"
SSH_ID="$FOLDER/key.pem"

echo "Build binary..."

dep ensure
go build -v -o distributed_alg cmd/distributedAlg/main.go || exit 1

echo "Copy binary and config on hosts..."

i=1
while read server; do
    echo "$i : $server"
    host=`cut -d':' -f1 <<<$server`
    port=`cut -d':' -f2 <<<$server`
    scp -i $SSH_ID -P $port "distributed_alg" "$host:" || exit 1
    scp -i $SSH_ID -P $port "$FOLDER/voisins-$i.txt" "$host:voisins.txt" || exit 1
    i=$(($i + 1))
done <"$SERVERS_FILE"

echo "Run algorithm..."

tmux -2 new-session -d -s $SESSION

# Setup a window for tailing log files
tmux split-window -v
tmux select-pane -t 1
tmux split-window -v
tmux select-pane -t 0
tmux split-window -h
tmux select-pane -t 2
tmux split-window -h
tmux select-pane -t 4
tmux split-window -h

i=0
while read server; do
    tmux select-pane -t $i
    host=`cut -d':' -f1 <<<$server`
    port=`cut -d':' -f2 <<<$server`
    tmux send-keys "ssh -i $SSH_ID -p $port $host" C-m

    if [ "$i" -eq "0" ]; then
        tmux send-keys 'clear && sleep 2 && ./distributed_alg 6000 voisins.txt INIT' C-m
    else
        tmux send-keys 'clear && ./distributed_alg 6000 voisins.txt WAIT' C-m
    fi

    i=$(($i + 1))
done <"$SERVERS_FILE"

tmux -2 attach-session -t $SESSION
